
#add the -static switch to CFLAGS to force static linking if desired
CPPFLAGS    += -I/usr/local/include -L/usr/local/lib -g
LDLIBS    += -L/usr/local/lib -largtable2

TARGETS = knitbit

all: $(TARGETS)

clean:
	rm -f core core.* *.o *~ $(TARGETS)




