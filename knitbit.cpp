/******************************************************************
 * Brad Hunting
 *
 * knitbit.cpp
 *
 * Creates a knitting pattern to encode a message with
 * knit and purl stitches.
 *
 ******************************************************************/

#include <iostream>
#include <argtable2.h>

#include <vector>
#include <bitset>
#include <string>
#include <string.h>


using namespace std;

int main(int argc, char* argv[])
{
   struct arg_int  *rowsperbit      = arg_int0("r","rowsperbit",NULL,"number of knitted rows per bit <2>");
   struct arg_int  *gauge           = arg_int0("g","gauge",NULL,"number of knitted rows per inch <4>");
   struct arg_dbl  *gaugeF          = arg_dbl0("G","gaugeF",NULL,"number of knitted rows per inch <4>");
   struct arg_int  *markincount     = arg_int0("i","markin",NULL,"number of lead in mark bits <10>");
   struct arg_int  *markoutcount    = arg_int0("o","markout",NULL,"number of trail out mark bits <7>");
   struct arg_int  *preambleCount   = arg_int0("p","preamble",NULL,"number of preamble bits <13>");
   struct arg_int  *startBitCount   = arg_int0("s","startbits",NULL,"number of start bits <1>");
   struct arg_int  *stopBitCount    = arg_int0("e","stopbits",NULL,"number of stop bits <2>");
   struct arg_int  *postambleCount  = arg_int0("t","postamble",NULL,"number of postamble bits <0>");
   struct arg_str  *message         = arg_str1("m","message","STRING","message to encode");
   struct arg_lit  *nochecksum      = arg_lit0("c","nochecksum", "turn off checksum after the string <ON>");
   struct arg_lit  *fliprows        = arg_lit0("f","fliprows", "emit even rows flipped <OFF>");
   struct arg_lit  *reverse         = arg_lit0("v","reverse", "emit reversed bit pattern <OFF>");
//   struct arg_lit  *knitstate       = arg_lit0("P","purl","set ON bit to purl <ON bit is knit>");


   struct arg_lit  *help    = arg_lit0("h","help", "print this help and exit");
   struct arg_end  *end     = arg_end(20);

   void* argtable[] = 
   {
      rowsperbit,
      gauge,
      gaugeF,
      markincount,
      markoutcount,
      preambleCount,
      startBitCount,
      stopBitCount,
      postambleCount,
      message,
      nochecksum,
      reverse,
      fliprows,
//      knitstate,
      help,
      end
   };
   const char* progname = "knitbits";
   int nerrors;
   int exitcode=0;
   int i, j, k;
   int linecount;

   /* verify the argtable[] entries were allocated sucessfully */
   if( arg_nullcheck(argtable) != 0 )
   {
      /* NULL entries were detected, some allocations must have failed */
      printf("%s: insufficient memory\n",progname);
      /* deallocate each non-null entry in argtable[] */
      arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
      return -1;
   }


   *rowsperbit->ival       = 2;
   *gauge->ival            = 4;
   *gaugeF->dval           = 4.0;
   *markincount->ival      = 10;
   *markoutcount->ival     = 7;
   *preambleCount->ival    = 13;
   *startBitCount->ival    = 1;
   *stopBitCount->ival     = 2;
   *postambleCount->ival   = 0;
   nochecksum->count       = 0;
   fliprows->count         = 0;
   reverse->count          = 0;
//   knitstate->count        = 0;


   /* Parse the command line as defined by argtable[] */
   nerrors = arg_parse(argc,argv,argtable);

   /* special case: '--help' takes precedence over error reporting */
   if( help->count > 0 )
   {
      printf("Usage: %s", progname);
      arg_print_syntax(stdout,argtable,"\n");
      printf("Print the knit / purl pattern for an ascii bit stream\n");
      arg_print_glossary(stdout,argtable,"  %-25s %s\n");
      /* deallocate each non-null entry in argtable[] */
      arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
      return -2;
   }

   /* If the parser returned any errors then display them and exit */
   if( nerrors > 0 )
   {
      /* Display the error details contained in the arg_end struct.*/
      arg_print_errors(stdout,end,progname);
      printf("Try '%s --help' for more information.\n",progname);
      arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
      return -3;
   }

   /* only get here is command line arguments were parsed sucessfully */

   printf("gauge          = %i\n", *gauge->ival);
   printf("gaugeF         = %f\n", *gaugeF->dval);
   printf("rowsperbit     = %i\n", *rowsperbit->ival);
   printf("markincount    = %i\n", *markincount->ival);
   printf("markoutcount   = %i\n", *markoutcount->ival);
   printf("preambleCount  = %i\n", *preambleCount->ival);
   printf("startBitCount  = %i\n", *startBitCount->ival);
   printf("stopBitCount   = %i\n", *stopBitCount->ival);
   printf("postambleCount = %i\n", *postambleCount->ival);
   printf("message        = %s\n", *message->sval);
   printf("nochecksum     = %i\n", nochecksum->count);
   printf("fliprows       = %i\n", fliprows->count);
   printf("reverse        = %i\n", reverse->count);


   int totalbitcount = (reverse->count ? 2 : 1) *
                       (
                       *markincount->ival +
                       *preambleCount->ival +
                       *startBitCount->ival +
                       *stopBitCount->ival +
                       *postambleCount->ival +
                       (nochecksum->count ? 0 : 8) +
                       *markoutcount->ival +
                       (strlen(*message->sval)*8) 
                       );

   int totalrowcount = *rowsperbit->ival * totalbitcount;

   std::cout << endl;
   std::cout << "Total Bit Count : " << totalbitcount << endl;
   std::cout << "Total Row Count : " << totalrowcount << endl;
   std::cout << endl;
   std::cout << "With a gauge of " << *gauge->ival << " rows per inch" << endl;
   std::cout << " total length will be approximately:" << endl;
   std::cout << "\t" << (int)(totalrowcount / *gauge->ival) << " inches" << endl;
   std::cout << "\t" << (int)(totalrowcount / *gauge->ival * 2.54) << " centimeters" << endl;
   std::cout << endl;
   std::cout << "With a gauge of " << *gaugeF->dval << " rows per inch" << endl;
   std::cout << " total length will be approximately:" << endl;
   std::cout << "\t" << (double)(totalrowcount / *gaugeF->dval) << " inches" << endl;
   std::cout << "\t" << (double)(totalrowcount / *gaugeF->dval * 2.54) << " centimeters" << endl;
   std::cout << endl;



   const int bitsetsize = 12000;

   std::bitset<bitsetsize> allbits;

   int bitpos = bitsetsize-1;

   // MARK IN
   for( i = 0; i < *markincount->ival; ++i )
   {
      allbits[bitpos--] = 1;
   }

   // PREAMBLE
   int bittoggle = 0;
   for( i = 0; i < *preambleCount->ival; ++i )
   {
      allbits[bitpos--] = bittoggle;
      bittoggle = !bittoggle;
   }

   // START BITS
   for( i = 0; i < *startBitCount->ival; ++i )
   {
      allbits[bitpos--] = 0;
   }

   // MESSAGE
   string msgstr = *message->sval;
   unsigned char checksumval = 0;

   for( string::iterator it = msgstr.begin(); it != msgstr.end(); ++it )
   {
      std::bitset<sizeof(char) * 8> binary(*it); //sizeof() returns bytes, not bits!
      std::cout << "Letter: " << *it << "\t";
      std::cout << "Hex: " << std::hex << (int)*it << "\t";
      std::cout << "Binary: " << binary << std::endl;

      checksumval += (unsigned char)*it & 0xFF;

      for( i = 8 /*sizeof(char)*/; i > 0; --i )
      {
         allbits[bitpos--] = (binary[i-1] ? 1 : 0);
      }
   }

   // CHECKSUM
   if( !nochecksum->count )
   {
      std::bitset<sizeof(char) * 8> binary(checksumval); //sizeof() returns bytes, not bits!
      std::cout << "Checksum: \t";
      std::cout << "Hex: " << std::hex << (int)checksumval << "\t";
      std::cout << "Binary: " << binary << std::endl;

      for( i = 8 /*sizeof(char)*/; i > 0; --i )
      {
         allbits[bitpos--] = (binary[i-1] ? 1 : 0);
      }

   }

   // STOP BITS
   for( i = 0; i < *stopBitCount->ival; ++i )
   {
      allbits[bitpos--] = 0;
   }

   // POSTAMBLE
   bittoggle = 0;
   for( i = 0; i < *postambleCount->ival; ++i )
   {
      allbits[bitpos--] = bittoggle;
      bittoggle = !bittoggle;
   }

   // MARKOUT
   for( i = 0; i < *markoutcount->ival; ++i )
   {
      allbits[bitpos--] = 1;
   }


   std::cout << endl;
   std::cout << endl;
   std::cout << endl;


//   std::cout << "allbits: " << allbits << endl;

   // RAW 0 & 1
   std::cout << "allbits: ";
   for( i = bitsetsize-1; i > bitpos; --i )
   {
      std::cout << allbits[i];
   }
   if( reverse->count )
   {
      std::cout << " || ";

      for( j = bitpos+1; j < bitsetsize; ++j )
      {
         std::cout << allbits[j];
      }
   }
   std::cout << endl;

   // * & - 
   std::cout << "allbits: ";
   for( i = bitsetsize-1; i > bitpos; --i )
   {
      std::cout << (allbits[i]? "*":"-");
   }
   if( reverse->count )
   {
      std::cout << " || ";

      for( j = bitpos+1; j < bitsetsize; ++j )
      {
         std::cout << (allbits[j]? "*":"-");
      }
   }
   std::cout << endl;

   // K & p
   std::cout << "allbits: ";
   for( i = bitsetsize-1; i > bitpos; --i )
   {
      std::cout << (allbits[i]? "K":"p");
   }
   if( reverse->count )
   {
      std::cout << " || ";

      for( j = bitpos+1; j < bitsetsize; ++j )
      {
         std::cout << (allbits[j]? "K":"p");
      }
   }
   std::cout << endl;



   // OUTPUT bit on new lines with count
//   linecount = 1;
//
//   std::cout << "allbits: " << allbits << endl;
//
//   // RAW 0 & 1
//   std::cout << "allbits: " << endl;
//   for( i = bitsetsize-1; i > bitpos; --i )
//   {
//      std::cout << dec << linecount++ << "\t";
//      std::cout << allbits[i] << "\t";
//      std::cout << (allbits[i]? "*":"-") << "\t";
//      std::cout << (allbits[i]? "K":"p") << endl;
//   }
//   if( reverse->count )
//   {
//      std::cout << endl << endl << " || " << endl << endl;
//
//      for( j = bitpos+1; j < bitsetsize; ++j )
//      {
//         std::cout << dec << linecount++ << "\t";
//         std::cout << allbits[j] << "\t";
//         std::cout << (allbits[j]? "*":"-") << "\t";
//         std::cout << (allbits[j]? "K":"p") << endl;
//      }
//   }
   std::cout << endl;



   // OUTPUT bit on new lines with count with Rows Per Bit
   linecount = 1;

//   std::cout << "allbits: " << allbits << endl;

   // RAW 0 & 1
   std::cout << "allbits expanded at " << *rowsperbit->ival << " rows per bit : " << endl;
   for( i = bitsetsize-1; i > bitpos; --i )
   {
      for( k = 0; k < *rowsperbit->ival; ++k )
      {
         std::cout << dec << linecount++ << "\t";
         std::cout << allbits[i] << "\t";
         std::cout << (allbits[i]? "*":"-") << "\t";
         std::cout << (allbits[i]? "K":"p") << endl;
      }
   }
   if( reverse->count )
   {
      std::cout << endl << " || " << endl << endl;

      for( j = bitpos+1; j < bitsetsize; ++j )
      {
         for( k = 0; k < *rowsperbit->ival; ++k )
         {
            std::cout << dec << linecount++ << "\t";
            std::cout << allbits[j] << "\t";
            std::cout << (allbits[j] ? "*":"-") << "\t";
            std::cout << (allbits[j] ? "K":"p") << endl;
         }
      }
   }
   std::cout << endl;


   
   if( fliprows->count )
   {
      // OUTPUT bit on new lines with count with Rows Per Bit with every other row flipped for scarves
      linecount = 1;

      // RAW 0 & 1
      std::cout << "allbits expanded at " << *rowsperbit->ival << " rows per bit with even rows flipped : " << endl;
      for( i = bitsetsize-1; i > bitpos; --i )
      {
         for( k = 0; k < *rowsperbit->ival; ++k )
         {
            std::cout << dec << linecount++ << "\t";
            if( 0 == (linecount % 2) )
            {
               std::cout << allbits[i] << "\t";
               std::cout << (allbits[i] ? "*":"-") << "\t";
               std::cout << (allbits[i] ? "K":"p") << endl;
            }
            else
            {
               std::cout << (allbits[i] ? "0" : "1") << "\t";
               std::cout << (allbits[i] ? "-":"*") << "\t";
               std::cout << (allbits[i] ? "p":"K") << endl;
            }
         }
      }
      if( reverse->count )
      {
         std::cout << endl << " || " << endl << endl;

         for( j = bitpos+1; j < bitsetsize; ++j )
         {
            for( k = 0; k < *rowsperbit->ival; ++k )
            {
               std::cout << dec << linecount++ << "\t";
               if( 0 == (linecount % 2) )
               {
                  std::cout << allbits[j] << "\t";
                  std::cout << (allbits[j] ? "*":"-") << "\t";
                  std::cout << (allbits[j] ? "K":"p") << endl;
               }
               else
               {
                  std::cout << (allbits[j] ? "0" : "1")<< "\t";
                  std::cout << (allbits[j] ? "-":"*") << "\t";
                  std::cout << (allbits[j] ? "p":"K") << endl;
               }
            }
         }
      }
      std::cout << endl;
   }


   std::cout << endl;
   std::cout << "Total Bit Count : " << totalbitcount << endl;
   std::cout << "Total Row Count : " << totalrowcount << endl;
   std::cout << endl;
   std::cout << "With a gauge of " << *gauge->ival << " rows per inch" << endl;
   std::cout << " total length will be approximately:" << endl;
   std::cout << "\t" << (int)(totalrowcount / *gauge->ival) << " inches" << endl;
   std::cout << "\t" << (int)(totalrowcount / *gauge->ival * 2.54) << " centimeters" << endl;
   std::cout << endl;
   std::cout << "With a gauge of " << *gaugeF->dval << " rows per inch" << endl;
   std::cout << " total length will be approximately:" << endl;
   std::cout << "\t" << (double)(totalrowcount / *gaugeF->dval) << " inches" << endl;
   std::cout << "\t" << (double)(totalrowcount / *gaugeF->dval * 2.54) << " centimeters" << endl;
   std::cout << endl << "NOTE: A standard scarf is 58 inches by 6 inches" << endl;
   std::cout << endl;


   /* deallocate each non-null entry in argtable[] */
   arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));
   return 0;
}

